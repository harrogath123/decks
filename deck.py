import pandas as pd
import json
import sqlite3
from pprint import pprint
from sqlalchemy import create_engine

#df= pd.read_json("RiveteersRampage_NCC.json")
#print(df)

#Create a SQLite engine
#engine = create_engine('sqlite:///tutorial.db')

con = sqlite3.connect('tutorial.db')
cursor = con.cursor()
timer = 6789

with open('RiveteersRampage_NCC.json') as f:
   json_data = f.read()

queries = [
    
   "CREATE TABLE IF NOT EXISTS commander (date DATE, code VARCHAR(50), artist VARCHAR(255), availability VARCHAR(255), BorderColor VARCHAR(255), ColorIdentity VARCHAR(255), colors VARCHAR(255), convertedManaCost DECIMAL, count INT, edhRank INT, finishes VARCHAR(255), language VARCHAR(255), multiverseId INT, name VARCHAR(255), text TEXT, type TEXT)",
   "CREATE TABLE IF NOT EXISTS cartes (frameversion INT, hasfoil INT, cardkingdomfoilId INT, hasnonfoil INT, cardspherefoilid INT, mcmId INT, mtgjsonV4 VARCHAR(255), isfoil INT, isstarter INT, keywords VARCHAR(255), layout VARCHAR(255), flavortest VARCHAR(255), edhrecSaltiness DECIMAL)",
   "CREATE TABLE IF NOT EXISTS formar (duel VARCHAR(255), legacy VARCHAR(255), oathbreaker VARCHAR(255), vintage VARCHAR(255))",
   "CREATE TABLE IF NOT EXISTS caractéristiques (manacost VARCHAR(255), maavalue DECIMAL, name VARCHAR(255), number INT, originaltext VARCHAR(1250), originaltype TEXT, power INT, printings VARCHAR(255), cardkingdom VARCHAR(255), cardkingdomfoil VARCHAR(255), rarity TEXT, ruling VARCHAR(255), date DATE, text VARCHAR(1250), setcode TEXT, foil TEXT, nonfoil TEXT, subtype TEXT, supertype TEXT, toughness TEXT, types TEXT, uuid TEXT, variations VARCHAR(255), watermat TEXT)"
]

try:
   for query in queries:
        cursor.execute(query)
        con.commit()
except sqlite3.Error as error:
  print(f"Error while working with SQLite: {error}")

con = sqlite3.connect('tutorial.db')
cursor = con.cursor()

with open('RiveteersRampage_NCC.json') as f:
    json_data = json.load(f)
pprint(json_data['data'].keys())
for k, v in json_data['data'].items():
    print(f"{k} : {len(v) if v else 0 }, {type(v)}")
print(json_data['data']['mainBoard'][0].keys())
for i, carte in enumerate(json_data['data']['mainBoard']):
    print(i, type(carte['isFoil']))

con = sqlite3.connect('tutorial.db')
cursor = con.cursor()

with open('SliverSwarm_CMM.json') as f:
    json_data = json.load(f)
pprint(json_data['data'].keys())
for k, v in json_data['data'].items():
    print(f"{k} : {len(v) if v else 0 }, {type(v)}")
print(json_data['data']['mainBoard'][0].keys())
for i, carte in enumerate(json_data['data']['mainBoard']):
    print(i, carte['name'])

con.commit()
con.close()

con = sqlite3.connect('tutorial.db')
cursor = con.cursor()

with open('VampiricBloodlust_C17.json') as f:
    json_data = json.load(f)

for card in json_data['data']['mainBoard']:
    if 'foreignData' in card:
        
        for foreign_data in card['foreignData']:
            try:
                cursor.execute("INSERT INTO commander (date, code, artist, availability, borderColor, colorIdentity, colors, convertedManaCost, count, finishes, language, multiverseId, name, type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                              (json_data['meta']['date'], json_data['data']['code'], card['artist'], ','.join(card['availability']), card['borderColor'], ','.join(card['colorIdentity']), ','.join(card['colors']), card['convertedManaCost'], card['count'],','.join(card['finishes']), foreign_data['language'], foreign_data['multiverseId'], foreign_data['name'], foreign_data['type']))
            except KeyError as e:
               print(f"Error accessing key: {e}")

con.commit(),
con.close()

con = sqlite3.connect('tutorial.db')
cursor = con.cursor()

with open('UrzaSIronAlliance_BRC.json') as f:
   json_data = json.load(f)

for card in json_data['data']['mainBoard']:
    try:

        duel = card['legalities']['duel']
        legacy = card['legalities']['legacy']
        oathbreaker = card['legalities']['oathbreaker']
        vintage = card['legalities']['vintage']
        cursor.execute("INSERT INTO formar (duel, legacy, oathbreaker, vintage) VALUES (?, ?, ?, ?)",
                      (duel, legacy, oathbreaker, vintage))
    except KeyError as e:
        print(f"Error accessing key: {e}")

con.commit()
con.close()

sqlite3.register_converter("BOOLEAN", lambda v: bool(int(v)))
con = sqlite3.connect('tutorial.db')
cursor = con.cursor()
timer = 6789
with open('VampiricBloodlust_C17.json') as f:
   json_data = f.read()

queries = [
"CREATE TABLE IF NOT EXISTS cartes (frameversion INT, hasfoil INT, cardkingdomfoilId INT, hasnonfoil INT, cardspherefoilid INT, mcmId INT, mtgjsonV4 VARCHAR(255), isfoil INT, isstarter INT, keywords VARCHAR(255), layout VARCHAR(255), flavortest VARCHAR(255), edhrecSaltiness DECIMAL)",
]
sqlite3.register_adapter(bool, int)
try:
   for query in queries:
        cursor.execute(query)
        con.commit()
 
except sqlite3.Error as error:
  print(f"Error while working with SQLite: {error}")

con = sqlite3.connect('tutorial.db')
cursor = con.cursor()
sqlite3.register_adapter(bool, int)
try:
    with open('BreedLethality_C16.json') as f:
        json_data = json.load(f)
except FileNotFoundError:
    print("The JSON file was not found.")
    con.close()
    exit()
except json.JSONDecodeError:
    print("Error decoding JSON file.")
    con.close()
    exit()
try:
    for card in json_data['data']['mainBoard']:
        if 'foreignData' in card:
            for foreign_data in card['foreignData']:
                try:
                    cursor.execute("""
                    INSERT INTO cartes (
                        frameversion, hasfoil, cardkingdomfoilId, hasnonfoil, cardspherefoilid, mcmId, mtgjsonV4, isfoil, isstarter, keywords, layout, flavortest, edhrecSaltiness
                    ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    """, (
                        json_data['meta']['date'], 
                        card.get('hasfoil', False), 
                        card.get('cardkingdomfoilId', None),  
                        card.get('hasnonfoil', False),  
                        card.get('cardspherefoilid', None),  
                        card.get('mcmId', None),  
                        card.get('mtgjsonV4', None),  
                        card.get('isfoil', False),  
                        card.get('isstarter', False),  
                        ','.join(card.get('keywords', [])),  
                        card.get('layout', None),  
                        card.get('flavortest', None),  
                        card.get('edhrecSaltiness', None)  
                    ))
                except KeyError as e:
                    print(f"Error accessing key: {e}")
                except sqlite3.Error as error:
                    print(f"Error inserting data: {error}")
    con.commit()
    print("Data inserted successfully.")
except sqlite3.Error as error:
    print(f"Error while inserting data: {error}")
con.close()