from fastapi import FastAPI, HTTPException
import sqlite3
app = FastAPI()

@app.get("/cartes")
async def cartes():
    try:
        con = sqlite3.connect('tutorial.db')
        cursor = con.cursor()
        cursor.execute("""
            SELECT name, COUNT(*) as occurrences
            FROM commander
            WHERE language = 'French'
            GROUP BY name
            ORDER BY occurrences DESC
            LIMIT 10
        """)
        rows = cursor.fetchall()
        if rows:
            result = [{"name": row[0], "occurrences": row[1]} for row in rows]
            return result
        else:
            raise HTTPException(status_code=404, detail="Aucune carte en français trouvée dans la base de données.")
    except sqlite3.Error as e:
        raise HTTPException(status_code=500, detail=f"Erreur de base de données: {e}")
    finally:
        if con:
            con.close()
#uvicorn main:app --reload

@app.get("/artiste")
async def artiste():
    try:
        con = sqlite3.connect('tutorial.db')
        cursor = con.cursor()
        cursor.execute("""
            SELECT artist, COUNT(*) as nombre_de_cartes
            FROM commander
            GROUP BY artist
            ORDER BY nombre_de_cartes DESC
            LIMIT 1
       """)
        row = cursor.fetchone()

        if row:
            artiste = row[0]
            nombre_de_cartes = row[1]
            return {"artiste": artiste, "nombre_de_cartes": nombre_de_cartes}
        else:
            raise HTTPException(status_code=404, detail="Aucun artiste trouvé dans la base de données.")

    except sqlite3.Error as e:
        raise HTTPException(status_code=500, detail=f"Erreur de base de données: {e}")

    finally:
        if con:
            con.close()

@app.get("/eleve")
async def carte_plus_cmc_eleve():
    try:
        con = sqlite3.connect('tutorial.db')
        cursor = con.cursor()
        cursor.execute("""
            SELECT DISTINCT name, convertedManaCost
            FROM commander
            WHERE language = 'French'
            ORDER BY convertedManaCost DESC
            LIMIT 1
        """)
        rows = cursor.fetchall()
        if rows:
            result = [{"name": row[0], "convertedManaCost": row[1]} for row in rows]
            return result
        else:
            raise HTTPException(status_code=404, detail="Aucune carte trouvée dans la base de données.")

    except sqlite3.Error as e:
        raise HTTPException(status_code=500, detail=f"Erreur de base de données: {e}")
    finally:
        if con:
           con.close()